import React from 'react'
import { View, Text, Button, StyleSheet, TextInput, FlatList, Alert } from 'react-native'

export default class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      count: 1,
      text: "",
      data: [],
      id: 0
    }
  }

  changeCount = () => {
    this.setState({ count: this.state.count + 1 })
  }

  render() {
    const uuidv4 = () => {
      return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
      });
    }
    const item = {
      id: uuidv4(),
      text: this.state.text
    }
    console.log(this.state.data);
    const handleAdd = () => {
      if (this.state.text !== '') return this.setState({ data: this.state.data.concat(item), text: '' })
      return Alert.alert("Vui long nhap du lieu")
    }



    return (
      <View style={styles.container}>

        <Text>{this.state.count}</Text>

        <View style={styles.but}>
          <Button
            title='+'
            onPress={this.changeCount} />
        </View>

        <Text>-------------------------</Text>

        <View style={styles.row}>
          <TextInput
            style={{ width: 250 }}
            spellCheck={false}
            placeholder='Nhap du lieu'
            value={this.state.text}
            onChangeText={(value) => this.setState({ text: value })}
          />
          <Text style={{ flex: 1 }}></Text>
          <Button
            title='ok'
            onPress={handleAdd}
          />
        </View>

        <FlatList
          data={this.state.data}
          renderItem={({ item }) => (<Text>{item.text}</Text>)}
          keyExtractor={item => item.id}
        />

      </View>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    marginTop: 15,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1
  },
  but: {
    width: 100
  },
  row: {
    paddingHorizontal: 8,
    flexDirection: 'row',
    alignSelf: 'flex-start',
  }
})

